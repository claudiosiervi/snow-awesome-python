# Snow Awesome Python

O objetivo desta lista é reunir referências de conteúdos técnicos como, bibliotecas, documentações, artigos, cursos, tutoriais, vídeos, livros, podcasts e quaisquer outros conteúdos pertinentes ao aprendizado e aprofundamento de cada tema. 

## Como contribuir

Sugira uma referência apenas se você já a utilizou antes, é importante manter o conteúdo sob curadoria para que essa lista não se torne apenas um *bookmark* de coisas interessantes que você quer salvar para o futuro.

As contribuições devem ser feitas através de *Merge Request* com uma breve explicação do conteúdo e motivação. 

As referências devem seguir o seguinte formato:

```
* [Nome](Link) - Breve explicação, author, ou informação relevante. #tipo 
```

Exemplos:

* [docs.python.org](https://docs.python.org) - Documentação oficial de Python. #doc


---

## Linguagem

* [docs.python.org](https://docs.python.org) - Documentação oficial de Python. #doc

## Boas Práticas

### Mensagens de commit
* [How to Write a Git Commit Message](https://chris.beams.io/posts/git-commit/) - Bom ponto de partida para começar a escrever mensagens de commit melhores. #tutorial #article
* [Karma Git Commit Msg](http://karma-runner.github.io/4.0/dev/git-commit-msg.html) - Gabarito para lembrar-se das infos importantes numa mensagem de commit. #doc
* [Preemptive commit comments](https://arialdomartini.wordpress.com/2012/09/03/pre-emptive-commit-comments/) - Escreva comentários de commit antes de codificar. Escreva o que o software deve fazer, não o que você fez. #article
* [pre-commit](https://pre-commit.com/) - Framework para gerenciar e manter hooks de pre-commit #doc

## Metaprogramação
## Algoritmos
## Design Patterns
## REST/OpenAPI
## GraphQL
## Sistemas Distribuídos
## Sistemas orientados a Eventos
## Arquitetura

## LGPD
* [Podcast - Hipsters.tech](https://hipsters.tech/lgpd-lei-geral-de-protecao-de-dados-hipsters-174/) - LGPD: Lei Geral de Proteção de Dados – Hipsters #174 #podcast

## Gestão de Configuração
* [Dynaconf](https://dynaconf.readthedocs.io/) - Sistema de configuração para aplicações Python - com suporte a aplicações de 12 fatores e extensões de Flask e Django #doc

## Gestão de Dependências
* [Poetry](https://python-poetry.org/) - Ferramenta para gerenciar projetos Python e suas dependências #doc

## Gestão de Credenciais e Certificados
* [Vault](https://www.vaultproject.io/) - Ferramenta para gerenciar secrets e proteger dados sensíveis #doc

## Gestão de Releases
## Gestão de Infraestrutura
## Gestão de crises
## Métricas
## Monitoramento
## CI/CD

## Testes
* [pytest](https://docs.pytest.org/) - Framework para escrever testes de forma fácil, desde testes unitários simples até testes funcionais complexos #doc

## Manutenção de software legado

## Análise Estática de Código
* [black](https://black.readthedocs.io/) - Formatador de código Python #doc
* [flake8](https://flake8.pycqa.org/) - Ferramenta para aplicação do Guia de Estilo Python #doc
* [mypy](https://mypy.readthedocs.io/) - Verificador de Tipagem Estática para Python #doc

## Análise de Vulnerabilidades
## UI/UX
## Design System

